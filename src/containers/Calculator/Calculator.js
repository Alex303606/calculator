import React,{Component} from 'react'
import './Calculator.css'
import Keyboard from "../../components/Keyboard/Keyboard";
import {connect} from 'react-redux'
class Calculator extends Component {
	render(){
		return (
			<div className="calculator">
				<input value={this.props.value} type="text"/>
				<Keyboard/>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		value: state.inputValue
	}
};

export default connect(mapStateToProps)(Calculator);