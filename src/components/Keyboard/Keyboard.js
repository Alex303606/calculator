import React, {Component} from 'react'
import './Keyboard.css'
import {connect} from 'react-redux'
import {back, buttonHandler, clear, getCalc, result} from "../../store/actions";

class Keyboard extends Component {
	render() {
		return (
			<div className="keyboard">
				{
					this.props.digits.map(digit => (
						<button key={digit} onClick={() => this.props.buttonHandler(digit)}>{digit}</button>
					))
				}
				<button onClick={() => this.props.getCalc('+')}>+</button>
				<button onClick={() => this.props.getCalc('-')}>-</button>
				<button onClick={() => this.props.getCalc('*')}>*</button>
				<button onClick={() => this.props.getCalc('/')}>/</button>
				<button onClick={this.props.result}>=</button>
				<button className="big" onClick={this.props.clear}>Clear</button>
				<button className="big" onClick={this.props.back}>Back</button>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		digits: state.digits
	}
};

const mapDispatchToProps = dispatch => {
	return {
		buttonHandler: (value) => dispatch(buttonHandler(value)),
		getCalc: (mark) => dispatch(getCalc(mark)),
		result: () => dispatch(result()),
		clear: () => dispatch(clear()),
		back: () => dispatch(back())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Keyboard);
