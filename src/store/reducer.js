import {BACK, CALC, CLEAR, ENTER, RESULT} from "./actions";

const initialState = {
	digits: [9, 8, 7, 6, 5, 4, 3, 2, 1, 0, '.'],
	inputValue: '',
	block: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ENTER:
			return {
				...state,
				inputValue: state.inputValue + action.value.toString()
			};
		case CALC:
			return {...state, inputValue: state.inputValue + action.mark, block: true};
		case RESULT:
			return {...state, block: false, inputValue: eval(state.inputValue)};
		case CLEAR:
			return {...state, inputValue: '', block: false};
		case BACK:
			return {...state, block: false, inputValue: state.inputValue.substring(0, state.inputValue.length - 1)};
		default:
			return state;
	}
};

export default reducer;